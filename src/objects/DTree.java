package objects;

import java.util.LinkedList;
import java.util.Queue;

import interfaces.IDTree;
import objects.DLine;

/**
 *This class should implement the interface <code>IDTree<DLine></code>.
 * <br>
 * <pre>
 * This class creates a binary (search) tree with nodes of type <code>DLine</code>. A DTree
 * follows these properties:
 * 		- It has a root DLine that is parent/ancestor of all nodes.
 * 		- For each DLine dl, the following conditions should always be held:
 * 			- dl.length > dl.leftChild.length
 * 			- dl.length <= dl.rightChild.length
 * </pre>
 * @author Azim Ahmadzadeh
 *
 */
public class DTree implements IDTree<DLine> {

	private DLine root;
	private int n;

	public DTree() {
		this.root = null;
		this.n = 0;
	}
	
	@Override
	public DLine getRoot() {
		return this.root;
	}
	
	@Override
	public int getNumberOfNodes() {

		return this.n;
	}

	@Override
	public void insert(DLine dl) {
		this.root = this.insertRec(this.root, dl);
		this.n++;
	}

	private DLine insertRec(DLine currentDLine, DLine dl) {

		// If this node does not exist
		if (currentDLine == null) {
			return new DLine(dl);
		}

		// If the new DLine is shorter than the current DLine,
		if (dl.length < currentDLine.length) {
			// add dl to the left of currentDLine
			currentDLine.setLeftChild(insertRec(currentDLine.getLeftChild(), dl));
		} else if (dl.length >= currentDLine.length) {
			// else, add dl to the right of currentDLine
			currentDLine.setRightChild(insertRec(currentDLine.getRightChild(), dl));
		}

		return currentDLine;
	}

	@Override
	public int getTreeLevel(DLine dl) {
		return 0;
	}
	
	@Override
	public void traverseInorder() {
		//TODO: call 'inorderRec' here
	}
	
	@Override
	public void traversePreorder() {
		//TODO: call 'preorderRec' here
	}
	
	@Override
	public void traversePostorder() {
		//TODO call 'postorderRec' here
	}
	
	@Override
	public void traverseLevelOrder() {
		//TODO
	}
	
	@Override
	public DLine find(DLine dl, int len) {
		//TODO
		return null;
	}
	
}
